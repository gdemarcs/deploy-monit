#!/usr/bin/python
DOCUMENTATION = '''
---
module: product_xml
short_description: Parse CERN product.xml files
description:
  Parses CERN's own deployment configuration file (XML) and extracts start script information for a specified application.
options: TODO
notes:
  - TODO
requirements: [ yaml, libxml2 ]
author: Gyorgy Demarcsek
'''

import sys
import os
import libxml2 as lxml

def get_start_script(xml, product=None, app=None, start_script=None):
    '''Finds path of start script of application app of product product. The application might have multiple start scripts specified, so it is also possible to address a <startscript> tag by name, within the application. It considers installLocation too, but if not found, it assumes the standard layout of /opt/$product/$application. Except for the <startscript> tag itself, it always uses the first tag that matches the criteria.'''
    tree = lxml.parseDoc(open(xml).read())
    app_node = None
    xpath_query = "//product"
    if product:
      xpath_query += ("[@name='%s']" % product)

    xpath_query += "/application"
    if app:
      xpath_query += ("[@name='%s']" % app)

    deploymentLocation = None
    app_node = tree.xpathEval(xpath_query)[0]
    if not app_node:
      return None

    product_node = app_node.xpathEval("..")[0]
    if app_node.xpathEval("./deployment"):
      deploymentLocation = app_node.xpathEval("./deployment")[0].xpathEval("@installLocation")[0].content

    if not deploymentLocation:
      deploymentLocation = os.path.join("/opt", product_node.xpathEval("@name")[0].content)
      if not os.path.exists(deploymentLocation):
        for entry in glob.glob("/opt/cern.*:%s:install" % product_node.xpathEval("@name")[0].content):
          if os.path.isdir(entry):
            deploymentLocation = entry
            break

      if not deploymentLocation:
        raise Exception("Could not figure out install location of the product based on product.xml")
      if app_node.xpathEval('@name'):
        deploymentLocation = os.path.join(deploymentLocation, app_node.xpathEval('@name')[0].content)

    xpath_query = "./startscript"
    if start_script:
      xpath_query += ("[@name='%s']" % start_script)

    start_script_nodes = app_node.xpathEval(xpath_query)
    if (len(start_script_nodes) > 1):
      raise Exception("It is not obvious which start script to use, please choose one: %s " % map(lambda n : n.xpathEval("@name")[0].content, start_script_nodes))

    start_script_node = None
    if len(start_script_nodes) > 0:
      start_script_node = start_script_nodes[0]

    if not start_script_node:
      if app_node.xpathEval(".//main-class"):
        return os.path.join(deploymentLocation, "bin", app_node.xpathEval('@name')[0].content.upper() + ".jvm")
      else:
        raise Exception("Could not find startscript for application")
    else:
      return os.path.join(deploymentLocation, "bin", start_script_node.xpathEval('@name')[0].content.upper() + ".jvm")

    return None

def main():
  '''Module entry point'''
  changed = True
  message = "Product.xml parsed successfully. "
  module = AnsibleModule(
      argument_spec = dict(
        path = dict(required=True, type='str'),
        product = dict(required=False, type='str'),
        application = dict(required=False, type='str'),
        pid_file = dict(required=False, type='str'),
        active = dict(required=False, type='bool', default=True),
        start_script = dict(required=False, type='str')
      )
  )


  start_script_path = get_start_script(module.params['path'], module.params['product'], module.params['application'], module.params['start_script'])

  if not start_script_path:
    module.fail_json(msg="Could not extract start script path from %s" % module.params['path'])

  new_service_dict = {"start_script": start_script_path, "active": module.params['active'] }
  if module.params['pid_file']:
    new_service_dict['pid_file'] = module.params['pid_file']

  if not os.path.exists(start_script_path):
    message += "Warning: Start script does not exist on this host: %s" % start_script_path

  module.exit_json(changed=changed, msg=message, result=new_service_dict)


from ansible.module_utils.basic import *
from ansible.module_utils.facts import *

if __name__ == "__main__":
  main()

