#!/usr/bin/python

# TODO

DOCUMENTATION = '''
---
module: yaml_diff
short_description: Compares YAML files.
description:
  Parses two YAML files and return changed status if and only if they differ semantically.
options:
  src:
    description:
      - Path of the first YAML file
    required: true
    default: null
  dest:
    description:
      - Path of the second YAML file
    required: true
    default: null
notes:
  - The module fails if any of the input files does not exist
  - The module fails if any of the input files can not be parsed as valid YAML
requirements: [ yaml ]
author: Gyorgy Demarcsek
'''

import yaml
import sys
import os

def main():
  changed = False
  message = "YAML files are identical"

  module = AnsibleModule(
      argument_spec = dict(
        src = dict(required=True, type='str'),
        dest = dict(required=True, type='str')
      )
  )

  if not os.path.exists(module.params['src']) or not os.path.exists(module.params['dest']):
    module.fail_json(msg="One of the files does not exist")

  try:
    A = open(module.params['src'])
    B = open(module.params['dest'])
    if yaml.load(A) != yaml.load(B):
      changed = True
      message = "YAML files differ"
  finally:
    A.close()
    B.close()

  module.exit_json(changed=changed, msg=message)

from ansible.module_utils.basic import *

if __name__ == "__main__":
  main()
