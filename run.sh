#!/bin/bash
if [[ $# -lt 1 || "$1" == "-h" ]]; then
  if [[ -z $EDITOR ]]; then
    EDITOR=nano
  fi
  TMP_FILE=$(mktemp)
  cp ./roles/monit/vars/main.yml $TMP_FILE
  $EDITOR $TMP_FILE
  if [[ $? -eq 0 ]]; then
    $0 $TMP_FILE
    rm $TMP_FILE
    exit $?
  fi
  exit 100
fi

export ANSIBLE_NOCOWS=1
ANSIBLE=$(which ansible-playbook)
INPUT_CONF_FILE=$1

if [[ $# -gt 1 ]]; then
  ANSIBLE_EXTRA_ARGS="${@:2}"
fi

$ANSIBLE $ANSIBLE_EXTRA_ARGS --tags monit -l $(hostname -s) -c local --extra-vars "input_conf=$INPUT_CONF_FILE" site.yml
