#!/usr/bin/python

'''
Thin wrapper around the Monit Deployment Playbooks written in Python.
It uses Ansible's Python API to trigger a play. It works with Ansible 1.9
and Python 2.6/2.7.

Gyorgy Demarcsek, CERN
'''

import ansible.runner
import ansible.playbook
from ansible import callbacks
from ansible import utils

import socket
import argparse
import getpass
import yaml
import os
import glob
import subprocess
import tempfile
import json
import sys
import shutil

# Runtime constants
CWD       = os.path.dirname(os.path.realpath(__file__))
USER      = getpass.getuser()
HOSTNAME  = socket.gethostname().split('.')[0]
TMPDIR    = "/tmp/monit-deploy/" #tempfile.mkdtemp()
EDITOR    = os.environ.get('EDITOR', 'nano')

def git_clone(url, target):
  '''Clones a Git repository identified by URL into the target directory'''
  runner = ansible.runner.Runner(
      module_name='git',
      module_args="repo=%s dest=%s force=yes accept_hostkey=yes" % (url, target),
      forks=10,
      pattern=HOSTNAME,
      transport='local'
  )
  ret = runner.run()['contacted']
  if not ret[HOSTNAME].has_key("changed"):
    raise Exception("Failed to clone git repository: %s" % ret[HOSTNAME])


def get_monit_instances(repo_dir):
  '''Constructrs a list of available Monit instances found in the repo_dir directory. It returns a dict, where keys
  are the names of the found Monit instances and values are paths to their corresponding deployment configuration file'''
  monits = {}
  for yml in glob.glob("%s/%s/%s/*.yml" % (repo_dir, USER, HOSTNAME)):
    with open(yml) as stream:
      yaml_data = yaml.load(stream)
      try:
        monits[yaml_data['monit_name']] = yml
      except:
        continue

  return monits

def find_monit_instance(repo_dir, name):
  '''It finds a Monit instance in repo_dir by name. If found, it returns the corresponding descriptor (YAML file path),
  otherwiese returns None'''
  monits = get_monit_instances(repo_dir)
  try:
    return monits[name]
  except KeyError:
    return None

def _check_run(*args, **kwargs):
  proc = subprocess.Popen(*args, **kwargs)
  proc.communicate()
  if proc.returncode != 0:
    raise Exception("Failed to execute command: %s" % args[0])

def main():
  # Parse arguments
  parser = argparse.ArgumentParser(description='Deploy Monit infrastructure')
  parser.add_argument('--name', required=False, help='Name of the Monit instance')
  parser.add_argument('--edit', required=False, help='Edit deployment configuration first', action='store_true')
  parser.add_argument('--from-file', help='Use custom deployment configuration file')
  parser.add_argument('--delete', help='Delete the given instance', action='store_true')
  args = parser.parse_args()

  # Check out Monit Instance Database
  git_clone("ssh://%s@cwe-513-vol069/opt/git/testrepo4.git" % USER, TMPDIR) # TODO: Use defaults.yml to get this URL

  # Set up Ansible callbacks and some other stuff
  stats = callbacks.AggregateStats()
  playbook_cb = callbacks.PlaybookCallbacks(verbose=utils.VERBOSITY)
  runner_cb = callbacks.PlaybookRunnerCallbacks(stats, verbose=utils.VERBOSITY)

  # Select deployment configuration
  monit_instance_yml = None
  if args.from_file:
    monit_instance_yml = args.from_file
  else:
    if not args.name:
      raise Exception("You must specify an instance name")
    monit_instance_yml = find_monit_instance(TMPDIR, args.name)

  update = True
  if not monit_instance_yml and not args.delete:
    monit_instance_yml = tempfile.mkstemp()[1]
    shutil.copy2(os.path.join(CWD, "roles", "monit", "vars", "main.yml"), monit_instance_yml)
    update = False
    print("Creating new instance")
  else:
    print("WARNING: Using existing configuration -- you may be overwriting it")
    if args.delete:
      if not monit_instance_yml:
        raise Exception("Instance does not exist")
      ans = "?"
      while not (ans.lower() in ["x", "y"]):
        ans = raw_input("Are you sure you want to delete configuration '%s' ? [y/n]" % args.name)
      if ans == "y":
        _check_run(["git", "reset", "--hard"], cwd=os.path.realpath(os.path.dirname(monit_instance_yml)))
        _check_run(["git", "rm", os.path.basename(monit_instance_yml)], cwd=os.path.realpath(os.path.dirname(monit_instance_yml)))
        _check_run(["git", "commit", "-m", "Deleted configuration: %s, %s, %s" % (args.name, monit_instance_yml, HOSTNAME)], cwd=TMPDIR)
        _check_run(["git", "push"], cwd=os.path.realpath(os.path.dirname(monit_instance_yml)))
        return 0

  # Let the user edit it if we've been asked to
  if args.edit:
    subprocess.call([EDITOR, monit_instance_yml])


  template_yaml = yaml.load(open(monit_instance_yml).read())
  with open(monit_instance_yml, "w+") as new_monit_instance:
    if not args.from_file and args.name:
      template_yaml["monit_name"] = args.name
    new_monit_instance.write(yaml.dump(template_yaml, encoding='utf-8'))

  sudo_password = None
  final_input_data = {}

  # See if we need elevated privileges
  with open(monit_instance_yml) as input_file:
    final_input_data = yaml.load(input_file)
    if final_input_data.get("monit_on_boot"):
      sudo_password = getpass.getpass("SUDO password: ")

  # Invoke Ansible with our playbook
  pb = ansible.playbook.PlayBook(
      playbook=os.path.join(CWD, "site.yml"),
      stats=stats,
      callbacks=playbook_cb,
      runner_callbacks=runner_cb,
      check=False,
      transport="local",
      host_list=[HOSTNAME],
      only_tags=['monit'],
      extra_vars={"input_conf": monit_instance_yml},
      become_pass=sudo_password,
      module_path="/etc/ansible/library"
  )

  result = pb.run()

  # Report result of play
  if int(result[HOSTNAME]["failures"]) > 0 or int(result[HOSTNAME]["unreachable"]) > 0:
    print("Deployment failed: %s" % monit_instance_yml)
    return 1
  else:
    print("Deployment of Monit configuration '%s' complete" % final_input_data["monit_name"])
    return 0

if __name__ == "__main__":
  try:
    lock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    lock.bind("\0postconnect_gateway_notify_lock")
  except socket.error:
    print("Locking error - process already running?")
    sys.exit(1)

  try:
    exit_code = main()
    sys.exit(exit_code)
  except Exception, e:
    print(e)
    sys.exit(100)
